This small library provides a minimal, easy to use API to write, read, and modify HDF5 files. 
The APIs support read and write access to groups and datasets and their attributes.
It  allows  the  use  of  all  the  main  Fortran  datatypes.   
The  support  for  the  COMPLEX  type is implemented just doubling the dimension 
in the first direction of the datasets andreading/writing them as real. 

COMPLEX and REAL types are currently assumed to be double precision.  


Strings are written as fixed length but the library is able to read 
also variable-length strings, which guarantees compatibility with files written by other codes.

The API provides descriptors for handling files, groups and datasets.  
Passing these descriptors the interfaces allow: opening and closing files and dataset; 
writing and read-ing attributes; writing and reading datasets; reading and writing hyperslabs.
